using System;

namespace HelloWorld
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("=====Invoice Header========"); 
      string Data=" ABC0001";
      string Name=" EC DATAGROUP ";
      double com=60000.00D;
      double num1=38000.50D;
      double num2=1999.50D;
      double num3=100000.00D;
      string num4="5%";
      double num5=5000.00D;
      string num6="3%";
      double num7=3150.00D;
      double tot=  101850.00D;
      Console.WriteLine("Invoice No:"+ Data);
      Console.WriteLine("Customer Name:"+ Name);
      Console.WriteLine("Invoice Date:9/9/2020 9:03:32 PM" );
      Console.WriteLine("=======Invoice Detail=====" );
      Console.WriteLine("Computers:"+com);
      Console.WriteLine("Service Charges:"+num1);
      Console.WriteLine("Other Charges:"+num2);
      Console.WriteLine("Sub Total:"+num3);
      Console.WriteLine("Tax:"+num4  +  "   "  +num5);
      Console.WriteLine("Discount:"+num6+ "    "+num7);
      Console.WriteLine("Total:"+tot);
      
    }
  }
}